package com.ssm.dao;


import com.ssm.entity.Resources;

import java.util.List;

public interface ResourcesDao {
    /**
     * 新增资源
     * @param re 资源实体类对象
     */
    public void insertResources(Resources re);

    /**
     * 修改资源
     * @param re 资源实体类对象
     */
    public void updateResources(Resources re);

    /**
     * 删除资源
     * @param ids 资源主键数组
     */
    public void deleteResources(Integer[] ids);

    /**
     * 条件查询资源
     * @param re 查询条件
     * @return 查询到的数据
     */
    public List<Resources> findResources(Resources re);

    /**
     * 根据主键查询资源
     * @param id 资源主键
     * @return 单个资源
     */
    public Resources findResourcesById(Integer id);

}

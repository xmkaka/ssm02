package com.ssm.dao;

import com.ssm.entity.Users;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UsersDao {
    /**
     * 新增用户
     * @param users 用户实体类对象
     */
    public void insertUsers(Users users);

    /**
     * 修改用户
     * @param users 用户实体类对象
     */
    public void updateUsers(Users users);

    /**
     * 删除用户
     * @param ids 用户主键数组
     */
    public void deleteUsers(Integer[] ids);

    /**
     * 条件查询用户
     * @param users 查询条件
     * @return 查询到的数据
     */
    public List<Users> findUsers(Users users);

    /**
     * 根据主键查询用户
     * @param id 用户主键
     * @return 单个用户
     */
    public Users findUsersById(Integer id);

    /**
     * 用户登录
     * @param username 用户名
     * @param password 密码
     * @return 用户对象
     */
    public Users loginUsers(@Param("username") String username, @Param("password") String password);

    /**
     * 用户名是否存在
     * @param username 用户名
     * @return 0不存在，非0存在
     */
    public int usernameIsExist(String username);

}

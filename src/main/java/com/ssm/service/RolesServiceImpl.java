package com.ssm.service;

import com.ssm.dao.RolesDao;
import com.ssm.entity.Roles;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("rolesService")
public class RolesServiceImpl implements RolesService{

    /**
     * 注入角色Dao
     */
    @Resource
    private RolesDao rolesDao;

    public void insertRoles(Roles roles) {
        rolesDao.insertRoles(roles);
    }

    public void updateRoles(Roles roles) {
        rolesDao.updateRoles(roles);
    }

    public void deleteRoles(Integer[] ids) {
        rolesDao.deleteRoles(ids);
    }

    public List<Roles> findRoles(Roles roles) {
        return rolesDao.findRoles(roles);
    }

    public Roles findRolesById(Integer id) {
        return rolesDao.findRolesById(id);
    }

    public List<Roles> findRolesAll() {
        return rolesDao.findRolesAll();
    }
}

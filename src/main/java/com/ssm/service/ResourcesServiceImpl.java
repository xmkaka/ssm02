package com.ssm.service;

import com.ssm.dao.ResourcesDao;
import com.ssm.entity.Resources;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("resourcesService")
public class ResourcesServiceImpl implements ResourcesService{

    /**
     * 注入资源Dao
     */
    @Resource
    private ResourcesDao resourcesDao;

    public void insertResources(Resources re) {
        resourcesDao.insertResources(re);
    }

    public void updateResources(Resources re) {
        resourcesDao.updateResources(re);
    }

    public void deleteResources(Integer[] ids) {
        resourcesDao.deleteResources(ids);
    }

    public List<Resources> findResources(Resources re) {
        return resourcesDao.findResources(re);
    }

    public Resources findResourcesById(Integer id) {
        return resourcesDao.findResourcesById(id);
    }
}

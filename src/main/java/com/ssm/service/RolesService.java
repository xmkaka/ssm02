package com.ssm.service;

import com.ssm.entity.Roles;

import java.util.List;

public interface RolesService {
    /**
     * 新增角色
     * @param roles 角色实体类对象
     */
    public void insertRoles(Roles roles);

    /**
     * 修改角色
     * @param roles 角色实体类对象
     */
    public void updateRoles(Roles roles);

    /**
     * 删除角色
     * @param ids 角色主键数组
     */
    public void deleteRoles(Integer[] ids);

    /**
     * 条件查询角色
     * @param roles 查询条件
     * @return 查询到的数据
     */
    public List<Roles> findRoles(Roles roles);

    /**
     * 根据主键查询角色
     * @param id 角色主键
     * @return 单个角色
     */
    public Roles findRolesById(Integer id);

    /**
     * 查询所有角色
     * @return 查询到的数据
     */
    public List<Roles> findRolesAll();
}

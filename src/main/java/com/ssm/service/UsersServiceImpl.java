package com.ssm.service;

import com.ssm.dao.UsersDao;
import com.ssm.entity.Users;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("usersService")
public class UsersServiceImpl implements UsersService{

    /**
     * 注入用户Dao
     */
    @Resource
    private UsersDao usersDao;

    public void insertUsers(Users users) {
        usersDao.insertUsers(users);
    }

    public void updateUsers(Users users) {
        usersDao.updateUsers(users);
    }

    public void deleteUsers(Integer[] ids) {
        usersDao.deleteUsers(ids);
    }

    public List<Users> findUsers(Users users) {
        return usersDao.findUsers(users);
    }

    public Users findUsersById(Integer id) {
        return usersDao.findUsersById(id);
    }

    public Users loginUsers(String username, String password) {
        return usersDao.loginUsers(username, password);
    }

    public int usernameIsExist(String username) {
        return usersDao.usernameIsExist(username);
    }

//    public void insertUsersRoles(Integer userid, Integer[] roleids) {
//        usersDao.deleteUsersRoles(userid);
//        for ( Integer roleid : roleids ) {
//            usersDao.insertUsersRoles(userid, roleid);
//        }
//    }
}

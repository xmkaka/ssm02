package com.ssm.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.entity.Resources;
import com.ssm.service.ResourcesService;
import com.ssm.util.JSONResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class ResourcesController {
    /**
     * 注入资源Service
     */
    @Resource
    private ResourcesService resourcesService;

    /**
     * 条件查询资源
     * @param re 资源实体对象
     * @param page 当前页面数
     * @param rows 每页条数
     * @return JSON响应数据
     */
    @GetMapping("/findResources")
    public JSONResult findResources(Resources re, @RequestParam(value = "page",defaultValue = "1") Integer page,
                                    @RequestParam(value = "rows",defaultValue = "5")Integer rows){
        PageHelper.startPage(page, rows);
        List<Resources> list = resourcesService.findResources(re);
        PageInfo<Resources> pageInfo = new PageInfo<Resources>(list);
        return JSONResult.ok(list, pageInfo.getTotal());
    }
    /**
     * 新增资源
     * @param re 资源实体对象
     * @return JSON响应数据
     */
    @PostMapping("/insertResources")
    public JSONResult insertResources(Resources re){
        resourcesService.insertResources(re);
        return JSONResult.ok();
    }
    /**
     * 修改资源
     * @param re 资源实体对象
     * @return JSON响应数据
     */
    @PostMapping("/updateResources")
    public JSONResult updateResources(Resources re){
        resourcesService.updateResources(re);
        return JSONResult.ok();
    }
    /**
     * 删除资源
     * @param ids 资源主键数组
     * @return JSON响应数据
     */
    @PostMapping("/deleteResources")
    public JSONResult deleteResources(@RequestParam("ids[]") Integer[] ids){
        resourcesService.deleteResources(ids);
        return JSONResult.ok();
    }

    /**
     * 根据主键查询资源
     * @param id 资源主键
     * @return JSON响应数据
     */
    @GetMapping("/findResourcesById")
    public JSONResult findResourcesById(Integer id){
        return JSONResult.ok(resourcesService.findResourcesById(id));
    }

}

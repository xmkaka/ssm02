package com.ssm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
    /**
     * 首页
     */
    @GetMapping("/index")
    public String index(){
        return "index";
    }

    /**
     * 登录
     */
    @GetMapping("/login")
    public String login(){
        return "login";
    }

    /**
     * 用户页面
     */
    @GetMapping("/users")
    public String users(){
        return "users";
    }

    /**
     * 角色页面
     */
    @GetMapping("/roles")
    public String roles(){
        return "roles";
    }

    /**
     * 资源页面
     */
    @GetMapping("/resources")
    public String resources(){
        return "resources";
    }
}

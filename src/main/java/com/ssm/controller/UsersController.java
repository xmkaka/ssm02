package com.ssm.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.entity.Users;
import com.ssm.service.UsersService;
import com.ssm.util.JSONResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class UsersController {
    /**
     * 注入用户Service
     */
    @Resource
    private UsersService usersService;

    /**
     * 条件查询用户
     * @param users 用户实体对象
     * @param page 当前页面数
     * @param rows 每页条数
     * @return JSON响应数据
     */
    @RequestMapping("/findUsers")
    public JSONResult findUsers(Users users, @RequestParam(value = "page",required = false,defaultValue = "1") Integer page,
                                @RequestParam(value = "pageSize",required = false,defaultValue = "5")Integer pageSize){
        PageHelper.startPage(page, pageSize);
        List<Users> list = usersService.findUsers(users);
        PageInfo<Users> pageInfo = new PageInfo<Users>(list);
        return JSONResult.ok(list, pageInfo.getTotal());
    }
    /**
     * 新增用户
     * @param users 用户实体对象
     * @return JSON响应数据
     */
    @RequestMapping("/insertUsers")
    public JSONResult insertUsers(Users users){
        usersService.insertUsers(users);
        return JSONResult.ok();
    }
    /**
     * 修改用户
     * @param users 用户实体对象
     * @return JSON响应数据
     */
    @RequestMapping("/updateUsers")
    public JSONResult updateUsers(Users users){
        usersService.updateUsers(users);
        return JSONResult.ok();
    }
    /**
     * 删除用户
     * @param ids 用户主键数组
     * @return JSON响应数据
     */
    @RequestMapping("/deleteUsers")
    public JSONResult deleteUsers(@RequestParam("ids[]") Integer[] ids){
        usersService.deleteUsers(ids);
        return JSONResult.ok();
    }

    /**
     * 根据主键查询用户
     * @param id 用户主键
     * @return JSON响应数据
     */
    @RequestMapping("/findUsersById")
    public JSONResult findUsersById(Integer id){
        return JSONResult.ok(usersService.findUsersById(id));
    }

    /**
     * 用户登录
     * @param username 用户名
     * @param password 密码
     * @param session 会话
     * @return 登录成功将用户对象存入会话中，否则错误提示
     */
    @RequestMapping("/loginUsers")
    public JSONResult loginUsers(String username, String password, HttpSession session){
        Users users = usersService.loginUsers(username, password);
        if(users==null){
            return JSONResult.error("用户名或密码有误");
        }
        session.setAttribute("loginUsers", users);
        return JSONResult.ok();
    }

    /**
     * 用户登出
     * @param session 会话
     * @return 登出成功
     */
    @RequestMapping("/logoutUsers")
    public JSONResult logoutUsers(HttpSession session){
        session.removeAttribute("loginUsers");
        session.invalidate();
        return JSONResult.ok();
    }

    /**
     * 判断用户名是否存在
     * @param username 用户名
     * @return 如果存在错误提示，否则可用
     */
    @RequestMapping("/usernameIsExist")
    public JSONResult usernameIsExist(String username){
        if(usersService.usernameIsExist(username)==0)
            return JSONResult.ok();
        else
            return JSONResult.error();
    }

    /**
     * 添加用户所属角色
     * @param userid 指定用户主键
     * @param roleids 指定角色主键数组
     */
//    @PostMapping("/insertUsersRoles")
//    public JSONResult insertUsersRoles(Integer userid,@RequestParam("roleids[]")Integer[] roleids){
//        usersService.insertUsersRoles(userid, roleids);
//        return JSONResult.ok();
//    }
}

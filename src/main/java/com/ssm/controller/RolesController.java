package com.ssm.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.entity.Roles;
import com.ssm.service.RolesService;
import com.ssm.util.JSONResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class RolesController {
    /**
     * 注入角色Service
     */
    @Resource
    private RolesService rolesService;

    /**
     * 条件查询角色
     * @param roles 角色实体对象
     * @param page 当前页面数
     * @param rows 每页条数
     * @return JSON响应数据
     */
    @GetMapping("/findRoles")
    public JSONResult findRoles(Roles roles, @RequestParam(value = "page",defaultValue = "1") Integer page,
                                @RequestParam(value = "rows",defaultValue = "5")Integer rows){
        PageHelper.startPage(page, rows);
        List<Roles> list = rolesService.findRoles(roles);
        PageInfo<Roles> pageInfo = new PageInfo<Roles>(list);
        return JSONResult.ok(list, pageInfo.getTotal());
    }
    /**
     * 新增角色
     * @param roles 角色实体对象
     * @return JSON响应数据
     */
    @PostMapping("/insertRoles")
    public JSONResult insertRoles(Roles roles){
        rolesService.insertRoles(roles);
        return JSONResult.ok();
    }
    /**
     * 修改角色
     * @param roles 角色实体对象
     * @return JSON响应数据
     */
    @PostMapping("/updateRoles")
    public JSONResult updateRoles(Roles roles){
        rolesService.updateRoles(roles);
        return JSONResult.ok();
    }
    /**
     * 删除角色
     * @param ids 角色主键数组
     * @return JSON响应数据
     */
    @PostMapping("/deleteRoles")
    public JSONResult deleteRoles(@RequestParam("ids[]") Integer[] ids){
        rolesService.deleteRoles(ids);
        return JSONResult.ok();
    }

    /**
     * 根据主键查询角色
     * @param id 角色主键
     * @return JSON响应数据
     */
    @GetMapping("/findRolesById")
    public JSONResult findRolesById(Integer id){
        return JSONResult.ok(rolesService.findRolesById(id));
    }

    /**
     * 查询所有角色
     * @return JSON响应数据
     */
    @GetMapping("/findRolesAll")
    public JSONResult findRolesAll() {
        return JSONResult.ok(rolesService.findRolesAll());
    }
}

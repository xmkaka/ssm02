package com.ssm.util;

public class LazySingleton {
    /**
     * 1. 私有化构造方法
     */
    private LazySingleton() {
    }
    /**
     * 2.定义一个私有的静态变量，类型就是此类类型
     */
    private static LazySingleton singleton;

    /**
     * 3.提供获取单例对象的公共的静态方法
     *  并在方法中创建此类唯一对象对象
     */
    public static synchronized LazySingleton getInstance(){
        if(singleton==null)
            singleton = new LazySingleton();
        return singleton;
    }
}

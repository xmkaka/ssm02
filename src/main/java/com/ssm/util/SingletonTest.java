package com.ssm.util;

public class SingletonTest {
    public static void main(String[] args) {
        /**
         * 单例模式：
         * 通过此设计模式定义的类，在整个应用程序中只有一个对象
         * 1.懒汉模式 LazySingleton
         * 2.饿汉模式 HungerSingleton
         */
//        HungerSingleton obj1 = new HungerSingleton();
        HungerSingleton obj1 = HungerSingleton.getInstance();
        System.out.println(obj1);
        HungerSingleton obj2 = HungerSingleton.getInstance();
        System.out.println(obj2);
        System.out.println(obj1 == obj2);

        System.out.println("---------------");

        LazySingleton obj3 = LazySingleton.getInstance();
        LazySingleton obj4 = LazySingleton.getInstance();
        System.out.println(obj3 == obj4);
    }
}

package com.ssm.util;

public class HungerSingleton {
    /**
     * 1. 私有化构造方法
     */
    private HungerSingleton() {
    }
    /**
     * 2.定义一个私有的静态属性，类型就是此类类型
     * 并创建该类的唯一对象，赋值给此属性
     */
    private static HungerSingleton singleton = new HungerSingleton();
    /**
     * 3.提供获取单例对象的公共的静态方法
     */
    public static HungerSingleton getInstance(){
        return singleton;
    }
}

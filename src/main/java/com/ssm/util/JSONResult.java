package com.ssm.util;

import java.util.List;

/**
 * 在异步请求中向浏览器响应的Java对象
 */
public class JSONResult {
    /**
     * 响应业务状态
     * 200：表示成功
     * 500：表示错误
     */
    private Integer status;
    /**
     * 响应消息
     */
    private String msg;
    /**
     * 响应的集合数据
     */
    private List rows;
    /**
     * 总条数，适应bootstrap-table插件
     */
    private Long total;
    /**
     * 响应的非集合数据
     */
    private Object data;
    /**
     * 异步验证
     */
    private boolean valid;

    public JSONResult() {
    }

    public JSONResult(Integer status, String msg, List rows, Long total, Object data, Boolean valid) {
        this.status = status;
        this.msg = msg;
        this.rows = rows;
        this.total = total;
        this.data = data;
        this.valid = valid;
    }

    public static JSONResult ok(){
        return new JSONResult(200,"OK",null,null,null,true);
    }
    public static JSONResult ok(String msg){
        return new JSONResult(200,msg,null,null,null,true);
    }

    /**
     * 分页查询时使用
     */
    public static JSONResult ok(List rows, Long total){
        return new JSONResult(200,"OK",rows,total,null,true);
    }

    /**
     * 单条数据查询时使用
     */
    public static JSONResult ok(Object data){
        return new JSONResult(200,"OK",null,null,data,true);
    }

    public static JSONResult error(){
        return new JSONResult(500,"ERROR",null,null,null,false);
    }

    public static JSONResult error(String msg){
        return new JSONResult(500,msg,null,null,null,false);
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List getRows() {
        return rows;
    }

    public void setRows(List rows) {
        this.rows = rows;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }
}

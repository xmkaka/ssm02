package com.ssm.entity;

import java.util.List;

public class Users {
    /**
     * 主键id
     */
    private Integer id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 真实姓名
     */
    private String realname;
    /**
     * 用户包含的角色列表
     */
//    private List<Roles> rolesList;
    /**
     * 用户包含的资源列表
     */
//    private List<Resources> resourcesList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

//    public List<Roles> getRolesList() {
//        return rolesList;
//    }
//
//    public void setRolesList(List<Roles> rolesList) {
//        this.rolesList = rolesList;
//    }
//
//    public List<Resources> getResourcesList() {
//        return resourcesList;
//    }
//
//    public void setResourcesList(List<Resources> resourcesList) {
//        this.resourcesList = resourcesList;
//    }
}

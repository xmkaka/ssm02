package com.ssm.entity;

public class Resources {
    /**
     * 主键id
     */
    private Integer id;
    /**
     * 资源名称
     */
    private String resourcesName;
    /**
     * 资源路径
     */
    private String url;
    /**
     * 资源排序
     */
    private Integer sort;
    /**
     * 资源类型
     */
    private String type;
    /**
     * 父资源
     */
    private Resources pre;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getResourcesName() {
        return resourcesName;
    }

    public void setResourcesName(String resourcesName) {
        this.resourcesName = resourcesName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Resources getPre() {
        return pre;
    }

    public void setPre(Resources pre) {
        this.pre = pre;
    }
}

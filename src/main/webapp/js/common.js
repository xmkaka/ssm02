$(function () {
    $(".modal").modal({
        backdrop: "static",
        show: false
    });
    $(".modal").on("hide.bs.modal", function () {
        $(this).find("form")[0].reset();
        $(this).find("form").data("bootstrapValidator").resetForm();
    });
    window.alert = function (message, title) {
        var model = "<div class='modal fade' id='modal_alert'>";
        model += "<div class='modal-dialog modal-sm'>";
        model += "<div class='modal-content'>";
        model += "<div class='modal-header'>";
        model += "<h4 class='modal-title' id='alert_title'></h4></div>";
        model += "<div class='modal-body'><div id='alert_msg'></div></div>";
        model += "<div class='modal-footer'>";
        model += "<button id='ok_btn' class='btn btn-sm btn-warning' data-dismiss='modal'>确 定</button>";
        model += "</div></div></div></div>";
        if($("#modal_alert")[0]){
            $("#modal_alert").empty();
            $("#modal_alert").remove();
        }
        $("body").append(model);
        $("#alert_title").html(title?title:"提示");
        $("#alert_msg").html(message);
        $("#modal_alert").modal({
            backdrop: "static",
            show: true
        });
    }
});
// alert("测试html对js的引入...");
//将表单中的数据转换为json对象
$.fn.serializeObject = function() {
    var obj = {};
    var array = this.serializeArray();
    $.each(array, function() {
        if (obj[this.name]) {
            if (!obj[this.name].push) {
                obj[this.name] = [ obj[this.name] ];
            }
            obj[this.name].push(this.value || '');
        } else {
            obj[this.name] = this.value || '';
        }
    });
    return obj;
}
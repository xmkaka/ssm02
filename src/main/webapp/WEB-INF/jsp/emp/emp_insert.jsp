<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <base href="<%=request.getContextPath()%>/">
    <title>Title</title>
    <!-- bootstrap核心样式文件 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
    <!-- jquery -->
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <!-- bootstrap中的js文件 -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.js" ></script>
</head>
<body>
<div class="container">
    <div style="text-align: center">
        <h2>员工新增</h2>
    </div>
    <form action="insertEmp" method="post" style="width: 400px;margin: 0px auto;">
        <div id="msg" style="color:red;"></div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">员工姓名</span>
                <input type="text" name="ename" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">岗位&emsp;&emsp;</span>
                <input type="text" name="job" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">管理者&emsp;</span>
                <input type="text" name="mgr" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">入职日期</span>
                <input type="date" name="hiredate" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">薪资&emsp;&emsp;</span>
                <input type="text" name="sal" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">奖金&emsp;&emsp;</span>
                <input type="text" name="comm" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">部门&emsp;&emsp;</span>
                <select name="dept.deptno" class="form-control">
                    <c:forEach items="${deptList}" var="dept">
                    <option value="${dept.deptno}">${dept.dname}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div style="text-align: right">
            <button type="submit" class="btn btn-primary">确定</button>
            <button type="reset" class="btn btn-default">取消</button>
        </div>
    </form>
</div>
</body>
</html>

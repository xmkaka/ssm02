<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <base href="<%=request.getContextPath()%>/">
    <title>Title</title>
    <!-- bootstrap核心样式文件 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
    <!-- jquery -->
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <!-- bootstrap中的js文件 -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.js" ></script>
</head>
<body>
<div class="container">
    <div style="text-align: center">
        <h2>员工修改</h2>
    </div>
    <form action="updateEmp" method="post" style="width: 400px;margin: 0px auto;">
        <input type="hidden" name="empno" value="${emp.empno}">
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">员工姓名</span>
                <input type="text" name="ename" class="form-control" value="${emp.ename}"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">岗位&emsp;&emsp;</span>
                <input type="text" name="job" class="form-control"  value="${emp.job}"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">管理者&emsp;</span>
                <input type="text" name="mgr" class="form-control"  value="${emp.mgr}"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">入职日期</span>
                <input type="date" name="hiredate" class="form-control"  value="<fmt:formatDate value="${emp.hiredate}" pattern="yyyy-MM-dd"/>"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">薪资&emsp;&emsp;</span>
                <input type="text" name="sal" class="form-control"  value="${emp.sal}"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">奖金&emsp;&emsp;</span>
                <input type="text" name="comm" class="form-control"  value="${emp.comm}"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">部门&emsp;&emsp;</span>
                <select name="dept.deptno" class="form-control">
                    <c:forEach items="${deptList}" var="dept">
                        <option value="${dept.deptno}" <c:if test="${dept.deptno==emp.dept.deptno}">selected</c:if>>${dept.dname}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div style="text-align: right">
            <button type="submit" class="btn btn-primary">确定</button>
            <button type="reset" class="btn btn-default">取消</button>
        </div>
    </form>
</div>
</body>
</html>

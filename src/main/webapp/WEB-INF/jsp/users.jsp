<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <base href="<%=request.getContextPath()%>/">
    <title>Title</title>
    <!-- bootstrap样式文件 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
    <!-- bootstrap-datetimepicker样式文件 -->
    <link rel="stylesheet" href="bootstrap-datetimepicker/css/bootstrap-datetimepicker.css" />
    <!-- bootstrap-table样式文件 -->
    <link rel="stylesheet" href="bootstrap-table/bootstrap-table.css" />
    <!-- bootstrap-validator样式文件 -->
    <link rel="stylesheet" href="bootstrap-validator/css/bootstrapValidator.css" />
    <!-- jquery -->
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <!-- bootstrap的js文件 -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
    <!-- bootstrap-datetimepicker的js文件 -->
    <script type="text/javascript"
            src="bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript"
            src="bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
    <!-- bootstrap-table的js文件 -->
    <script type="text/javascript" src="bootstrap-table/bootstrap-table.js"></script>
    <script type="text/javascript" src="bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
    <!-- bootstrap-validator的js文件 -->
    <script type="text/javascript" src="bootstrap-validator/js/bootstrapValidator.js"></script>
    <script type="text/javascript" src="bootstrap-validator/js/language/zh_CN.js"></script>
    <script type="text/javascript" src="js/common.js"></script>
    <style type="text/css">
        .modal-dialog {
            width: 350px;
            background-color: ;
        }
    </style>
    <script type="text/javascript">

    </script>
</head>
<body>
<!--===================页面内容=========================-->
<div class="panel-body">
    <!--=============toolbar工具栏=================-->
    <div id="toolbar" class="btn-group">
        <button id="insertBtn" type="button" class="btn btn-success" data-toggle="modal" data-target="#in_win">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>添加
        </button>
        <button id="updateBtn" type="button" class="btn btn-warning" data-toggle="modal" data-target="#up_win">
            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>编辑
        </button>
        <button id="delBtn" type="button" class="btn btn-danger">
            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>删除
        </button>
        <button id="rolesBtn" type="button" class="btn btn-primary">
            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>角色分配
        </button>
    </div>
    <!--=============toolbar工具栏 .end=================-->
    <!--=============table表格=================-->
    <table id="tab" <%--class="table table-hover table-bordered table-striped"--%>></table>
    <!--=============table表格 .end=================-->
</div>
<!--=================页面内容 .end====================-->
<!--===================弹出窗体=====================-->
<div class="modal fade" id="in_win">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" ><span>&times;</span></button>
                <h2 class="modal-title">用户新增</h2>
            </div>
            <div class="modal-body">
                <form id="in_form">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">用户名&emsp;</span>
                            <input type="text" id="in_username" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">密码&emsp;&emsp;</span>
                            <input type="text" id="in_password" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">真实姓名</span>
                            <input type="text" id="in_realname" class="form-control"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>取消</button>
                <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span>确定</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="up_win">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" ><span>&times;</span></button>
                <h2 class="modal-title">用户修改</h2>
            </div>
            <div class="modal-body">
                <form id="up_form">
                    <input type="hidden" id="up_userid"/>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">用户名&emsp;</span>
                            <input type="text" id="up_username" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">密码&emsp;&emsp;</span>
                            <input type="text" id="up_password" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">真实姓名</span>
                            <input type="text" id="up_realname" class="form-control"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>取消</button>
                <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span>确定</button>
            </div>
        </div>
    </div>
</div>
<!--===================弹出窗体 .end=====================-->
</body>
</html>

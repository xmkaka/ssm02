<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <base href="<%=request.getContextPath()%>/">
    <title>Title</title>
    <!-- bootstrap核心样式文件 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
    <!-- jquery -->
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <!-- bootstrap中的js文件 -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.js" ></script>
</head>
<body>
<div class="container">
    <div style="text-align: center">
        <h2>部门新增</h2>
    </div>
    <form action="insertDept" method="post" style="width: 400px;margin: 0px auto;">
        <div id="msg" style="color:red;"></div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">部门名称</span>
                <input type="text" name="dname" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">部门位置</span>
                <input type="text" name="loc" class="form-control"/>
            </div>
        </div>
        <div style="text-align: right">
            <button type="submit" class="btn btn-primary">确定</button>
            <button type="reset" class="btn btn-default">取消</button>
        </div>
    </form>
</div>
</body>
</html>

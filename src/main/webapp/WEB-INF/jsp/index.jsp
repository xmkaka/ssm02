<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <base href="<%=request.getContextPath()%>/">
    <title>Title</title>
    <!-- bootstrap核心样式文件 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
    <!-- jquery -->
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <!-- bootstrap中的js文件 -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.js" ></script>
    <style type="text/css">
        * {
            margin: 0px;
            padding: 0px;
        }
        html,body {
            height: 100%;
        }
        /*顶部栏样式*/
        .navbar {
            background-color: #3498DB;
            margin-bottom: 0px;
            height: 10%;
        }
        .title {
            color: #fff;
            font: bold 30px "楷体";
        }
        .white-font {
            color: #fff;
        }
        .header .glyphicon {
            font-size: 22px;
        }
        /*左侧导航*/
        .left-accordion{
            float: left;
            width: 15%;
            height: 87%;
            background-color: #3498DB;
        }
        .left-accordion .btn{
            width: 100%;
        }
        /*右侧iframe*/
        .right-content {
            float: left;
            width: 85%;
            height: 85%;
        }
        .right-content iframe {
            width: 100%;
            height: 100%;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            getCurTime();
        });
        function getCurTime(){
            var date = new Date();
            var html = date.getFullYear();
            html += "-";
            html += (date.getMonth()+1+"").length==1 ? "0"+(date.getMonth()+1) : date.getMonth()+1;
            html += "-";
            html += (date.getDate()+"").length==1 ? "0"+date.getDate() : date.getDate();
            html += " ";
            html += (date.getHours()+"").length==1 ? "0"+date.getHours() : date.getHours();
            html += ":";
            html += (date.getMinutes()+"").length==1 ? "0"+date.getMinutes() : date.getMinutes();
            html += ":";
            html += (date.getSeconds()+"").length==1 ? "0"+date.getSeconds() : date.getSeconds();
            $("#systime").html(html);

            setTimeout(getCurTime, 1000);
        }
    </script>
</head>
<body>
<!--=========================header顶栏=============================-->
<div class="header">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu">
                    <span class="sr-only">折叠按钮</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><span class="title">信息管理系统</span></a>
            </div>

            <div class="collapse navbar-collapse" id="menu">
                <ul class="nav navbar-nav">
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a><span class="glyphicon glyphicon-user white-font"></span>&nbsp;<span class="white-font">欢迎登录，${loginUsers.username}</span></a></li>
                    <li><a><span class="glyphicon glyphicon-time white-font"></span>&nbsp;<span class="white-font" id="systime"></span></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="white-font">设置</span><span class="glyphicon glyphicon-cog white-font"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#"><span class="glyphicon glyphicon-lock"></span>&nbsp;修改密码</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="logout" onclick="return confirm('确定退出吗?')"><span class="glyphicon glyphicon-off"></span>&nbsp;退出系统</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>

<!--header .end-->
<!--=========================left-accordion左侧菜单=============================-->
<div class="left-accordion">
    <div class="panel-group">
        <a class="btn btn-info" data-toggle="collapse" href="#emp">人员管理</a>
        <div id="emp" class="panel-collapse collapse in">
            <div class="list-group">
                <a href="#" target="contentFrame" class="list-group-item">员工列表</a>
                <a href="#" target="contentFrame" class="list-group-item">部门列表</a>
            </div>
        </div>
        <!-- emp .end -->
        <a class="btn btn-info" data-toggle="collapse" href="#sys">系统管理</a>
        <div id="sys" class="panel-collapse collapse in">
            <div class="list-group">
                <a href="html/users.html" target="contentFrame" class="list-group-item">用户列表</a>
            </div>
        </div>
        <!-- sys .end -->
    </div>
    <!-- panel-group .end -->
</div>
<!--leftaccordion .end-->
<!--=========================right-content右侧内容=============================-->
<div class="right-content">
    <iframe name="contentFrame" frameborder="0" scrolling="auto"></iframe>
</div>
<!--right-content .end-->
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <base href="<%=request.getContextPath()%>/">
    <title>Title</title>
    <!-- bootstrap核心样式文件 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
    <!-- jquery -->
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <!-- bootstrap中的js文件 -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.js" ></script>
    <style type="text/css">
        body {
            background-color: #3498DB;
        }
        .panel-body {
            width: 300px;
            margin: 70px auto;
            background-color: #fff;
            border-radius: 5px;
        }
    </style>
</head>
<body>
<div class="panel-body">
    <div style="text-align: center">
        <h2>用户登录</h2>
    </div>
    <form action="login" method="post">
        <div class="form-group has-feedback">
            <input type="text" name="username" class="form-control" placeholder="用户名"/>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="密码"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group" style="text-align: center">
            <button type="submit" class="btn btn-primary form-control">登&emsp;&emsp;&emsp;&emsp;录</button>
        </div>
    </form>
</div>
</body>
</html>

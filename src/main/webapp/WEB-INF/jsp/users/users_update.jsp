<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <base href="<%=request.getContextPath()%>/">
    <title>Title</title>
    <!-- bootstrap核心样式文件 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
    <!-- jquery -->
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <!-- bootstrap中的js文件 -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.js" ></script>
</head>
<body>
<div class="container">
    <div style="text-align: center">
        <h2>用户修改</h2>
    </div>
    <form action="updateUsers" method="post" style="width: 400px;margin: 0px auto;">
        <input type="hidden" name="userid" value="${users.userid}">
        <div class="form-group ">
            <div class="input-group">
                <span class="input-group-addon">用户名&emsp;</span>
                <input type="text" name="username" class="form-control" value="${users.username}" readonly/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">密码&emsp;&emsp;</span>
                <input type="text" name="password" class="form-control" value="${users.password}"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">真实姓名</span>
                <input type="text" name="realname" class="form-control" value="${users.realname}"/>
            </div>
        </div>
        <div style="text-align: right">
            <button type="submit" class="btn btn-primary">确定</button>
            <button type="reset" class="btn btn-default">取消</button>
        </div>
    </form>
</div>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <base href="<%=request.getContextPath()%>/">
    <title>Title</title>
    <!-- bootstrap核心样式文件 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
    <!-- jquery -->
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <!-- bootstrap中的js文件 -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.js" ></script>
    <script type="text/javascript">
        var sub = false;
        $(function(){
            $("#username").blur(function(){
                checkUsername();
            });
        });
        function checkUsername(){
            $.ajax({
                url : "checkUsername",
                type : "post",
                data : {
                    username : $("#username").val()
                },
                success : function(data){
                    if(data=="ok"){
                        $("#msg").html("放心使用");
                        sub = true;
                    }else{
                        $("#msg").html("当前用户名无法使用");
                        sub = false;
                        $("#username")[0].focus();
                    }
                }
            });
        }
    </script>
</head>
<body>
<div class="container">
    <div style="text-align: center">
        <h2>用户新增</h2>
    </div>
    <form action="insertUsers" method="post" style="width: 400px;margin: 0px auto;" onsubmit="return sub;">
        <div id="msg" style="color:red;"></div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">用户名&emsp;</span>
                <input type="text" id="username" name="username" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">密码&emsp;&emsp;</span>
                <input type="text" name="password" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">真实姓名</span>
                <input type="text" name="realname" class="form-control"/>
            </div>
        </div>
        <div style="text-align: right">
            <button type="submit" class="btn btn-primary">确定</button>
            <button type="reset" class="btn btn-default">取消</button>
        </div>
    </form>
</div>
</body>
</html>

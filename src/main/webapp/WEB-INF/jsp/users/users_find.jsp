<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <base href="<%=request.getContextPath()%>/">
    <title>Title</title>
    <!-- bootstrap核心样式文件 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
    <!-- jquery -->
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/common.js"></script>
    <!-- bootstrap中的js文件 -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.js" ></script>

    <script type="text/javascript">
        function showUpdate(){
            var id_box = $(":checkbox[name='ids']:checked");
            if(id_box.length==0){
                alert("请选择一条要修改的数据");
            }else if(id_box.length>1){
                alert("只能修改一条数据");
            }else{
                location.href = "showUpdateUsers?userid="+id_box.val();
            }
        }
        function del(){
            var id_box = $(":checkbox[name='ids']:checked");
            if(id_box.length==0){
                alert("请选择一条要删除的数据");
            }else{
                if(confirm("确定要删除吗?"))
                    $("#delForm")[0].submit();
            }
        }
    </script>
</head>
<body>
    <div class="container-fluid">
        <div style="margin: 10px 0px;">
            <a href="showInsertUsers" class="btn btn-default">新增</a>
            <a href="javascript:showUpdate();" class="btn btn-default">修改</a>
            <a href="javascript:del();" class="btn btn-default">删除</a>
        </div>
        <div style="height: 300px">
            <form id="delForm" action="deleteUsers" method="post">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th width="5%"><input type="checkbox" id="all"></th>
                        <th width="30%">用户名</th>
                        <th width="35%">密码</th>
                        <th width="30%">真实姓名</th>
                    </tr>
                </thead>
                <tbody id="tab">
                    <c:forEach items="${info.list}" var="users">
                    <tr>
                        <td><input type="checkbox" name="ids" value="${users.userid}"></td>
                        <td>${users.username}</td>
                        <td>${users.password}</td>
                        <td>${users.realname}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            </form>
        </div>
        <div style="text-align: right; margin-right: 15px;">
            <ul class="pagination">
                <li><a href="findUsers"><span >首页</span></a></li>
                <c:choose>
                    <c:when test="${info.pageNum==1}">
                        <li class="disabled"><span >上一页</span></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="findUsers?page=${info.prePage}"><span >上一页</span></a></li>
                    </c:otherwise>
                </c:choose>
                <c:forEach items="${info.navigatepageNums}" var="p">
                    <c:choose>
                        <c:when test="${info.pageNum==p}">
                            <li class="active"><a href="findUsers?page=${p}">${p}</a></li>
                        </c:when>
                        <c:otherwise>
                            <li><a href="findUsers?page=${p}">${p}</a></li>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <c:choose>
                    <c:when test="${info.pageNum==info.pages}">
                        <li class="disabled"><span >下一页</span></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="findUsers?page=${info.nextPage}"><span >下一页</span></a></li>
                    </c:otherwise>
                </c:choose>
                <li><a href="findUsers?page=${info.pages}"><span >末页</span></a></li>
            </ul>
        </div>
    </div>
</body>
</html>
